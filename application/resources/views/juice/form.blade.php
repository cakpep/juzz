
<form id="form-juice">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	    <h4 class="modal-title" id="myModalLabel">Choose Your Juice</h4>
	</div>
	<div class="modal-body">
	    <div class="container">
	        <div class="row">
	            <div class="row">
	                <div class="form-group col-md-6">
	                    <div class="row">
	                        <div class="col-md-8">
	                            <input type="text" placeholder="What fruite you want.. " name="fruite" class="form-control"/>
	                        </div>
	                        <div class="col-md-4">
	                            <input type="text" placeholder="How Much ?? " name="number_fruite" class="form-control"/>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="form-group col-md-6" >
	                    <textarea name="additional" class="form-control" placeholder="Additional.." ></textarea>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    <button id="btn-juice-submit" class="btn btn-primary">Submit</button>
	</div>
</form>