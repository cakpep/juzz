@extends('layouts.app')

@section('content')

<script src="js/socket.io-1.3.4.js"></script>

<div class="col-lg-12">
    <div class="col-lg-12 margin-tb">

        <!-- <a href="{{ URL('juices?modal=1') }}" data-toggle="modal" data-target="#create-juice" class="btn btn-primary">New Juice</a> -->
        <button data-toggle="modal"  data-target="#create-juice" class="btn btn-success">New Juice</button>
        <div class="pull-right">
            <h1>Juice Tracking Management System</h1>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <table id="table-juice" class="table table-bordered pagin-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Fruit</th>
                <th>Number Of Juice</th>
                <th>Additional</th>
                <th width="220px">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($listJuices as $juice_key => $juice)
            <tr>
                <td>{{ $juice->id or "" }}</td>
                <td>{{ $juice->fruite or "-" }}</td>
                <td>{{ $juice->number_fruite or "-" }}</td>
                <td>{{ $juice->additional or "-" }}</td>
                <td>
                    <button data-toggle="modal"  data-target="#create-todo" class="btn btn-primary">Edit</button>
                    <button  class="btn btn-danger">Delete</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $listJuices->links() !!}
</div>

<!-- Create Modal -->
<div class="modal fade" id="create-juice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <form method="POST" name="addTodo" role="form" action="juices" id="form-juice"> -->
            <form id="form-juice">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Choose Your Juice</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <div class="row">
                                    <div class="col-md-8">
                                        <input type="text" placeholder="What fruite you want.. " name="fruite" class="form-control"/>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="How Much ?? " name="number_fruite" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" >
                                <textarea name="additional" class="form-control" placeholder="Additional.." ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn-juice-submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script src="realtime/JsController/JuiceController.js"></script>
@endsection