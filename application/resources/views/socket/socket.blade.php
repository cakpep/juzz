@extends('layouts.app')

@section('content')
<script src="js/socket.io-1.3.4.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2" >
              <div id="messages" ></div>
              <div id="time" ></div>
            </div>
        </div>
    </div>
    <script>
        var socket = io.connect('http://localhost:8890');
        socket.on('chat', function (data) {
            console.log(data);
            var messages = JSON.parse(data);
            $( "#messages" ).append( "<p>"+messages.message+"</p>" );
            $( "#time" ).append( "<p>"+messages.time+"</p>" );
          });
    </script>


@endsection