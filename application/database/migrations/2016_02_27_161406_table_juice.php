<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class TableJuice extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('juice', function (Blueprint $table) {
			$table->increments('id');
			$table->string('fruite');
			$table->integer('number_fruite')->nullable();
			$table->text('additional')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop("juice");
	}
}
