<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juice extends Model {

	protected $table = 'juice';
	protected $fillable = [
		'fruite',
		'number_fruite',
		'additional',
		'created_at',
		'updated_at',
	];
	protected $message;

	public function setMessages($message, $type = 'info') {
		$this->message = [
			'type' => $type,
			'message' => $message,
		];
	}

	public function getMessages() {
		return $this->message;
	}

}
