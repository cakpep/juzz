<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use LRedis;
use Request;

class SocketController extends Controller {
	public function __construct() {
		$this->middleware('guest');
	}

	public function index() {
		return view('socket.socket');
	}
	public function writemessage() {
		return view('socket.writemessage');
	}
	public function sendMessage(Request $request) {
		$redis = LRedis::connection();
		$messages = [
			'message' => Request::input('message'),
			'time' => date('H:i:s'),
		];
		$redis->publish('chat', json_encode($messages));
		return redirect('writemessage');
	}
}