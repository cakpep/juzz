<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Juice;
use LRedis;
use Request;

class JuiceController extends Controller {
	private $redis;

	public function __construct() {
		$this->redis = LRedis::connection();
		$this->middleware('guest');
	}

	public function index() {
		// Request $request;
		if (!array_key_exists('modal', $_GET)) {
			$listJuices = Juice::paginate(15);
			return view('juice.index', [
				'listJuices' => $listJuices,
			]);
		} else {
			return view('juice.form');
		}
	}

	public function create() {
		if (Request::isMethod('POST')) {
			$juice = new Juice();
			$juice->fruite = Request::input('fruite');
			$juice->number_fruite = intval(Request::input('number_fruite'));
			$juice->additional = Request::input('additional');
			if ($juice->save()) {
				$this->redis->publish('juice', $juice->toJson());
				$juice->setMessages(
					'Juice With ' . $juice->fruite . ' Fruite, with Total ' . $juice->number_fruite . ' Is Created With Successfully',
					'success'
				);
			} else {
				$juice->setMessages(
					'Juice is Failed added.',
					'danger'
				);
			}
			echo json_encode($juice->getMessages());
		}
	}

	public function get($id) {
		return Juice::find($id)->toJson();
	}

	public function update(Request $request, $id) {
		$input = $request->all();
		Juice::where("id", $id)->update($input);
		$item = Juice::find($id);
		return $juice->toJson();
	}

	public function delete($id) {
		return Juice::where('id', $id)->delete();
	}

}
