
$(document).ready(function(){

    $("#btn-juice-submit").click(function(e){
        e.preventDefault();
        var postData = $('#form-juice').serializeArray();
        $.ajax({
            url: "http://localhost/riset/nodeJS/laravel-realtime/juices",
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR)
            {
                var data = JSON.parse(data);
                if(data.type=='success'){
                    $('#create-juice').modal('hide');
                    document.getElementById("form-juice").reset();
                }else{

                }
                // console.log(data);
            }
        });
    });

    var socket = io.connect('http://localhost:8890');
    socket.on('juice', function (data) {
        var juice = JSON.parse(data);
        var table_juice_row =   '<tr>\
                                    <td>'+juice.id+'</td>\
                                    <td>'+juice.fruite+'</td>\
                                    <td>'+juice.number_fruite+'</td>\
                                    <td>'+juice.additional+'</td>\
                                    <td>\
                                        <button data-toggle="modal"  data-target="#create-todo" class="btn btn-primary">Edit</button>\
                                        <button  class="btn btn-danger">Delete</button>\
                                    </td>\
                                </tr>';
        $('#table-juice > tbody:last-child').append(table_juice_row);
    });

});
