$(document).ready(function () {

	$('[data-toggle="modal"]').click(function(e) {
		e.preventDefault();
		var url = $(this).attr('href');              
		$.get(url, function(data) {        
			$( ".modal-content" ).html( data );
		});
	});

});