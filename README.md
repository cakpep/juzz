# Realtime App (Laravel 5.2 + NodeJS  + Ajax)

### Requirements : 
>
1. install nodejs
>
2. redis server
>
3. setup local .env
>
### How To Run : ###
>
* change directori into application. run **composer install**
>
* migrate table into database. run **php artisan migrate**
>
* run nodejs server on directori realtime/mynodejs/main.js
>
* to run server nodejs using command **node main.js**
>
>
### Note : ###
>
change url ajax on JuiceController.js with your url
>